import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getDatabase,onValue,ref,get,set,child,update,remove }
    from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS,uploadBytes,getDownloadURL }
    from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyCk2VSxRGhFFIjaLcyn-uuVNxiZYO7WYqs",
    authDomain: "pcpartes-c91d2.firebaseapp.com",
    projectId: "pcpartes-c91d2",
    storageBucket: "pcpartes-c91d2.appspot.com",
    messagingSenderId: "249612717328",
    appId: "1:249612717328:web:b7e44b363992f2ad3f6995"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

var btnIniciar = document.getElementById('iniciar');
var btnRegistro = document.getElementById('registrar');

var login = document.getElementById('login');
var formulario = document.getElementById('formulario');
var Contraseña;
var usuario = document.getElementById("usuario").value;
var contraseña = document.getElementById("contraseña").value;
var verificador = document.getElementById("verificador").value;
var Token;
function inicioSesion() {


    usuario = document.getElementById("usuario").value;
    contraseña = document.getElementById("contraseña").value;
    if (usuario == "ADMIN" && contraseña == "ADMIN") {
        alert("BIENVENIDO ADMINISTRADOR.");
        window.location.href = "registroP.html";
    }
    else {
        alert("USUARIO O CONTRASEÑA ERRONEA");
    }
}


function buscarDatos() {

    leerInputs();
    const dbref = ref(db);
    get(child(dbref, 'usuarios/' + usuario)).then((snapshot) => {
        if (snapshot.exists()) {

            Contraseña = snapshot.val().Contraseña;
            Token = snapshot.val().Token;
            comprobar();

        }
        else {
            alert("Usuario O Contraseña Incorrectos");
        }
    }).catch((error) => {
        alert("ERROR" + " " + error);
    });

}

function verificar() {
    leerInputs();
    if (usuario == "" || contraseña == "") {
        alert("INSERTE ALGUN VALOR");
    } else {
        buscarDatos();
    }
}
function comprobar() {
    if (contraseña == Contraseña) {
        alert("Bienvenido!")
        llenarInput();
        invisible();
    }
    else {
        alert("Usuario O Contraseña Incorrectos")
    }
}
function invisible() {
    login.style.display = "none";
    formulario.style.display = "block";

}
function llenarInput() {
    document.getElementById('verificador').value = Token;
}
function leerInputs() {
    usuario = document.getElementById("usuario").value;
    contraseña = document.getElementById("contraseña").value;
}
btnIniciar.addEventListener('click', verificar);